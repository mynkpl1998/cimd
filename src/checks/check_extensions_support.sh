#! /bin/bash

FILE="src/checks/extensions.txt"

while IFS='' read -r line || [[ -n "$line" ]]; do
    CPU_DUMP_INFO_COUNT="$(grep -ic $line /proc/cpuinfo)"
    
    if [ $CPU_DUMP_INFO_COUNT -gt 1 ]
    then
        echo "$line Support ... OK"
    else
        echo "$line Support ... Not Found"
        exit 1
    fi
done < "$FILE"

exit 0