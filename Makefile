CC=g++
CFLAGS=-W

EXTENSION_CHECK_SCRIPT = src/checks/check_extensions_support.sh

all: ${EXTENSION_CHECK_SCRIPT}
	bash ${EXTENSION_CHECK_SCRIPT}
	